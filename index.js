const express = require('express');
const app = express();
const PORT = 22022;

const adonisConfig = require('./adonisConfig');
let apiConfig = require('./apiConfig');
const _async = require('async');
const CronJob = require('cron').CronJob;
const request = require('request');
const util = require('util');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('adonis.db');
const faker = require('faker');
const moment = require('moment');
const FormData = require('form-data');
const crewProfile = require("./crewProfile");
let processed = {};
let duplicateUsers = [];

db.run('create table if not exists adonisUserStore_v2 (teammemberNumber TEXT, sequenceno TEXT, user TEXT)', function(err,results) {
    if (err) {
        console.log(err);
    }

    else {
        console.log('table exists');
    }
});

Object.defineProperty(Array.prototype, 'flat', {
    value: function(depth = 1) {
      return this.reduce(function (flat, toFlatten) {
        return flat.concat((Array.isArray(toFlatten) && (depth>1)) ? toFlatten.flat(depth-1) : toFlatten);
      }, []);
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text({ type: 'text/html; charset=utf-8' }));

function apolloToOkta(filter) {
    return new Promise(function(resolve,reject) {
        const options = {
            url: `https://apollosolutiononboardscheduletestapi.azurewebsites.net/api/OnBoardSchedule/EmployeeOnBoardScheduleSegments?employeeId=${filter}`,
            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
        };
    
        request.get(options, function(err,res) {
            if (err) {
                //console.log('got some apollo err');
               // reject(err);
               resolve(null);
            }

            else {
                if (res.statusCode == 401) {
                    refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                        const options = {
                            url: `https://apollosolutiononboardscheduletestapi.azurewebsites.net/api/OnBoardSchedule/EmployeeOnBoardScheduleSegments?employeeId=${filter}`,
                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
                        };                
                        request.get(options, function(err,res) {
                            if (err) {
                                reject(err);
                            }

                            else if (res.statusCode == 400) {
                                
                                resolve(null);
                            }
                
                            else {
                                const payload = res.body ? JSON.parse(res.body) : '';
                                resolve(payload);
                            }
                        });
                    });
                }
                //user doesnt exist
                else if (res.statusCode == 400) {
                    resolve(null);
                }

                else {
                    const payload = res.body ? JSON.parse(res.body) : '';
                    resolve(payload);
                }
            }
        });
    });
}

function getDataForKafka(user) {
    return new Promise(function(resolve,reject) {
        let newCrew = new crewProfile(user);
        apolloToOkta(user.teammemberNumber).then(function(data) {
            let duties = [];
            //found user
            if (data) {
                for (let i = 0; i < data.length; i++) {
                    let duty = {
                        "positionKey": data[i].positionKey,
                        "position": data[i].position,
                        "serviceLocationKey": data[i].serviceLocationKey,
                        "serviceLocationName": data[i].serviceLocationName,
                        "positionServiceLocation": data[i].positionServiceLocation,
                        "segmentStartDateTime": data[i].segmentStartDateTime,
                        "segmentEndDateTime": data[i].segmentEndDateTime,
                        "segmentType": data[i].segmentType,

                        "startDate": data[i].segmentStartDateTime,
                        "endDate": data[i].segmentEndDateTime,
                        "locationCode": data[i].serviceLocationKey,
                        "dutyLocation": data[i].serviceLocationName,
                        "dutyName": data[i].position,
                        "dutyCode": data[i].positionKey
                    };
                    duties.push(duty);
                }
            }

            else {
                duty = {
                    "startDate": "2020-04-11T09:04:00",
                    "endDate": "2021-04-26T08:04:00",
                    "locationCode": "45012",
                    "dutyLocation": "Korean BBQ",
                    "dutyName": "Asst. Waiter/ess",
                    "dutyCode": "329864"
                };
                duties.push(duty);
            }
            newCrew.pl.positionServiceLocation = duties[0].dutyName + ' ' + duties[0].dutyLocation;
            newCrew.pl.teamMemberDuties = duties;
            let splitVisas = [];
            const filtered = newCrew.pl.visas.filter(visa => visa.visaTypeCode != "");
            const split = filtered.map(function(visa) {
                if (visa.issueDate.indexOf('|') !== -1) {
                    visa.visaTypeCode = visa.visaTypeCode.split('|');
                    visa.number = visa.number.split('|');
                    visa.issueCountryCode = visa.issueCountryCode.split('|');
                    visa.issueDate = visa.issueDate.split('|');
                    visa.expiryDate =  visa.expiryDate.split('|');
                    console.log('process123', visa);
                    for (let i = 0; i < visa.issueDate.length; i++) {
                        let splitVisa = {
                            "visaTypeCode": visa.visaTypeCode[i],
                            "number": visa.number[i],   
                            "issueCountryCode": visa.issueCountryCode[i],    
                            "issueDate": moment(visa.issueDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(visa.issueDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS'),
                            "expiryDate": moment(visa.expiryDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(visa.expiryDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS'),
                            "numberOfEntries":null,         
                            "mediaItemId":null,         
                            "isApproved":true
                        };
                        splitVisas.push(splitVisa);
                    }

                    return splitVisas;
                }

                else {
                    visa.issueDate = moment(visa.issueDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(visa.issueDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS');
                    visa.expiryDate = moment(visa.expiryDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(visa.expiryDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS');
                    splitVisas.push(visa);
                    return visa;
                }
            });
            /***
             * Uncomment the following line to re-enable visa distro to kafka
             */
            /*newCrew.pl.visas = splitVisas;*/
            newCrew.pl.visas = [];
            if (newCrew.pl.identifications[0].number.indexOf('|') !== -1) {
                let allPassports = [];
                console.log('here');
                newCrew.pl.identifications[0].number = newCrew.pl.identifications[0].number.split('|');
                //country code only has one val
                //newCrew.pl.identifications[0].issueCountryCode = newCrew.pl.identifications[0].issueCountryCode.split('|');
                newCrew.pl.identifications[0].expiryDate = newCrew.pl.identifications[0].expiryDate.split('|');
                for (let i = 0; i < newCrew.pl.identifications[0].number.length; i++) {
                    let splitPass = {
                        "number": newCrew.pl.identifications[0].number[i],
                        //"issueCountryCode": newCrew.pl.identifications[0].issueCountryCode[i],
                        "issueCountryCode": newCrew.pl.identifications[0].issueCountryCode,
                        "expiryDate": moment(newCrew.pl.identifications[0].expiryDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(newCrew.pl.identifications[0].expiryDate[i], 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS')
                    };
                    allPassports.push(splitPass);
                }
                console.log('pp', allPassports);
                newCrew.pl.identifications = allPassports;
            }

            else {
                newCrew.pl.identifications[0].expiryDate = moment(newCrew.pl.identifications[0].expiryDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS') == 'Invalid date' ? null : moment(newCrew.pl.identifications[0].expiryDate, 'MM/DD/YYYY').format('YYYY-MM-DDTHH:MM:SS');
            }
            
            //console.log('nnc',newCrew);
            resolve(newCrew);
        });          
    }).catch(err => console.log('rejected here', err));
}

function refreshApolloToken(refreshToken) {
    return new Promise(function(resolve,reject) {
        const rp = require('request-promise');
        let apiConfig = require('./apiConfig.json');
        if (!!refreshToken) {
            const options = {
                method: 'POST',
                uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyRefreshToken',
                body: {
                    "clientId": apiConfig.apollo.clientID,
                    "clientSecret": apiConfig.apollo.clientSecret,
                    "companyCode": apiConfig.apollo.companyCode,
                    "refreshToken": apiConfig.apollo.refreshToken
                },
                json: true,
                headers: {"accept": "application/json"}
            };
             
            rp(options)
                .then(function (parsedBody) {
                    apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                    apiConfig.apollo.accessToken = parsedBody.accessToken;
                    resolve(parsedBody.accessToken);
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    
        else {
            const options = {
                method: 'POST',
                uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyAuth',
                body: {
                    "clientId": apiConfig.apollo.clientID,
                    "clientSecret": apiConfig.apollo.clientSecret,
                    "companyCode": apiConfig.apollo.companyCode
                },
                json: true,
                headers: {"accept": "application/json"}
            };
             
            rp(options)
                .then(function (parsedBody) {
                    apiConfig.apollo.accessToken = parsedBody.accessToken;
                    apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                    resolve(parsedBody.accessToken);
                })
                .catch(function (err) {
                    reject(err);
                });
        }
    });
}

function refreshAdonisToken() {
    return new Promise(function(resolve,reject) {
        const options = {
            url: "http://10.101.224.2/AdonisWebServices_VirginVoyages/CrewPortalWebService.svc/GNL_API_AUTHENTICATION",
            headers: {"Content-Type":"application/json"},
            body: JSON.stringify(adonisConfig)
        };

        request.post(options, function(err,res) {
            const body = JSON.parse(res.body);
            if (err) {
                console.log('err', err);
                reject(err);
            }

            else {
                apiConfig.adonis.authToken = body.GNL_API_AUTHENTICATIONResult.Authentication_Token;
                resolve(apiConfig.adonis.authToken);
            }
        });
    });
}

function distributeToKafka(crewMember) {
    return new Promise(function(resolve,reject) {
        const shipLookup = {
            "Scarlet Lady": "SC"
        };
    
        const countryCodeConverter = {
            "CS": "RS"
        };
    
        const marital = {
            "M":"2",
            "S":"1",
            "HOH":"1",
            "MFJ":"2",
            "MFS":"2"
        };
        let isCheckedIn = true;
        if ((crewMember.planned == 'N' && crewMember.debarkDate != '') || crewMember.planned == 'Y') {
            isCheckedIn = false;
        }

        else {
            isCheckedIn = true;
        }
        
        crewMember.passport_issued_country = countryCodeConverter[crewMember.pass_issueCountryCode] == undefined ? crewMember.pass_issueCountryCode : countryCodeConverter[crewMember.pass_issueCountryCode];
        crewMember.country_of_birth = countryCodeConverter[crewMember.country_of_birth] == undefined ? crewMember.country_of_birth : countryCodeConverter[crewMember.country_of_birth];
        crewMember.country_of_residence = countryCodeConverter[crewMember.addresscountryCode] == undefined ? crewMember.addresscountryCode : countryCodeConverter[crewMember.addresscountryCode];
        crewMember.country_of_nationality = countryCodeConverter[crewMember.citizenshipCountrycode] == undefined ? crewMember.citizenshipCountrycode : countryCodeConverter[crewMember.citizenshipCountrycode];
        const mxpUsers = ["Assistant Beverage Operations Manager", "Beverage Operations Manager", "Chief Financial Accountant", "Hotel Controller", "Revenue Analyst", "Senior Financial Analyst", "Executive Chef", "Executive Pastry Chef", "Executive Sous Chef", "Senior Executive Chef", "Captain", "Deputy Captain", "Safety Officer", "Chief Engineer", "Deputy Chief Engineer", "Entertainment Director", "Food and Beverage Director", "Hotel Administrator", "Hotel Director", "Hotel Director Experience", "Assistant Executive Housekeeper", "Executive Housekeeper", "Laundry Manager", "Assistant Casino Manager", "Assistant Retail Manager", "Assistant Shore Things Manager", "Assistant Wellness Manager", "Retail Manager", "Shore Things Manager", "Wellness Manager", "Clearance Officer", "Crew Service Manager", "Security Officer", "Casino Manager", "Hotel Director Operations", "Sailor Service Manager" ];
        let isMXPUser = mxpUsers.find(position => position == crewMember.Position);
        let departure_date; 
        if (crewMember.debarkeDate == '' && crewMember.estimateSignOffDate == '') {
            departure_date = "2021-01-01";
        }

        else {
            departure_date = crewMember.debarkeDate == '' ? "2021-01-01" : moment(crewMember.estimateSignOffDate).format('YYYY-MM-DD'); 
        }
        const payload = {
            "crew": [{
                "charge_id": crewMember.chargeID || "1",
                "first_name": crewMember.firstName,
                "middle_name": crewMember.middle_name,
                "last_name": crewMember.lastName,
                "gender": crewMember.gender || "UU",
                "title": crewMember.titleCode || "UU",
                "place_of_birth": crewMember.place_of_birth || "UU",
                "date_of_birth": crewMember.birthDate == undefined ? "1970-01-01" : moment(crewMember.birthDate).format('YYYY-MM-DD'),
                "country_of_birth": crewMember.country_of_birth || "UU",
                "country_of_residence": crewMember.country_of_residence || "UU",
                "country_of_nationality": crewMember.country_of_nationality || 'UU',
                "marital_status_id": marital[crewMember.maritalStatus] == undefined ? 6 : marital[crewMember.maritalStatus],
                "is_person_active": true,
                "installation_code": shipLookup[crewMember.Vessel],
                "room_nr": crewMember.stateroom == '' ? "0000" : crewMember.stateroom,
                "arrival_date": crewMember.embarkDate == '' ? "1970-01-01" : moment(crewMember.embarkDate).format('YYYY-MM-DD'),
                "departure_date": departure_date,
                "PIN": crewMember.pin,
                "person_primary_email": crewMember.email,
                "mxp_user": isMXPUser == undefined ? false : true,
                "position_name": crewMember.Position == '' ? "unknown" : crewMember.Position,
                "passport_type": 1,
                "passport_number": crewMember.pass_visaNumber == '' ? "unknown" : crewMember.pass_visaNumber,
                "passport_first_name": crewMember.firstName,
                "passport_last_name": crewMember.lastName,
                "passport_issued_by": crewMember.passport_issued_by == '' ? "unknown" : crewMember.passport_issued_by,
                "passport_issued_country": crewMember.country_of_nationality,
                "passport_issued_city": crewMember.passport_issued_city == '' ? "unknown" : crewMember.passport_issued_city,
                "passport_issued_date": crewMember.pass_issueDate == '' ? "1970-01-01" : moment(crewMember.pass_issueDate, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                "passport_expiration_date": crewMember.pass_expiryDate == '' ? "1970-01-01" : moment(crewMember.pass_expiryDate, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                "passport_is_primary": true,
                "embarkation_city_code": crewMember.embarkCity || "GOA",
                "debarkation_city_code": crewMember.debarkCity || "MIA",
                "is_checked_in": isCheckedIn,
                "manifest_type": "E"
            }]
        };
    
        console.log('pl', JSON.stringify(payload, null, 4));
        const options = {
            url: "http://10.101.224.114/API/MXP_Virgin.exe/crew",
            headers: {"Content-Type":"application/json"},
            body: JSON.stringify(payload)
        };
        request.post(options, function(err, res) {
            if (err) {
                console.log(err);
                return done(err, null);
            }
            else {
                if (res.statusCode == 200) {
                    resolve([null, `The following user was successfully sent to Kafka: ${crewMember.pin}`]);
                }
    
                else {
                    console.log('retrying');
                    request.post(options, function(err, res) {
                        if (err) {
                            console.log(err);
                            return done(err, null);
                        }
                        else {
                            if (res.statusCode == 200) {
                                resolve([null, `The following user was successfully sent to Kafka: ${crewMember.pin}`]);
                            }
                
                            else {
                                console.log('pin', crewMember.pin);
                                console.log('rc', res.statusCode);
                                console.log('rb' ,res.body);
                                resolve([null, `The following user failed to Kafka: ${crewMember.pin}`]);
                            }
                        }
                    });
                }
            }
        });
    });
}

function getAdonisUsers(filter) {
    return new Promise(function(resolve,reject) {
        console.log('getting adonis users');
        let payloadBody = {
            "request": {
                "Authentication_Token": apiConfig.adonis.authToken,
                "View":"PW001SRV22",
                "Filter":"Vessel='Scarlet Lady'"
            }
        };

        if (filter) {
          payloadBody.request.Filter = "email='" + filter[1] + "'";  
        }

        let options = {
            url: "http://10.101.224.2/AIWS_VirginVoyages/AdonisIntegrationWebService.svc/GNL_APMCrewListViews",
            headers: {"Content-Type":"application/json"},
            body: JSON.stringify(payloadBody)
        };

        request.post(options, function(err,res) {
            const body = JSON.parse(res.body);
            
            if (err) {
                reject(err);
            }

            //token is expired
            else if (!body.GNL_APMCrewListViewsResult.Authentication_Approved) {
                refreshAdonisToken().then(function(token) {
                    //update body with new auth token
                    payloadBody.request.Authentication_Token = token;
                    options.body = JSON.stringify(payloadBody);
                    request.post(options, function(err,res) {
                        const body = JSON.parse(res.body);
                        if (err) {
                            reject(err);
                        }
            
                        else {
                            //console.log('adsadsad',body)
                            //console.log('about to return' + JSON.parse(body.GNL_APMCrewListViewsResult.Result).length + 'users');
                            resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
                        }
                    });
                });
            }

            else {
                //console.log('about to return' + JSON.parse(body.GNL_APMCrewListViewsResult.Result).length + 'users1');
                resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
            }
        });
    });
}

function getDelta(user, done) {
    db.get(`select * from adonisUserStore_v2 where teammemberNumber='${user.teammemberNumber}' AND sequenceno='${user.sequenceno}'`, function(err,row) {
        if (err) {
            console.log(err);
            return done(err,null);
        }
    
        else {
            //this is a new user, I need to go to the adonis photo API and get the user photo
            if (row === undefined) {
                for (let prop in user) {
                    if (!!user[prop] && user[prop].indexOf('\'') !== -1) {
                        console.log(`found something ${user[prop]}`)
                        user[prop] = [user[prop].slice(0,user[prop].indexOf('\'')), "'", user[prop].slice(user[prop].indexOf('\''))].join('');
                    }
                }
                let userString = JSON.stringify(user);
                userString = encodeURI(userString);
                db.get(`insert into adonisUserStore_v2 (teammemberNumber, sequenceno, user) values('${user.teammemberNumber}', '${user.sequenceno}', '${userString}')`, function(err,row) {                            
                    if (err) {
                        console.log('get an err inserting ' + user.teamMemberNumber);
                        console.log(err);
                        return done(err,null);
                    }
                
                    else {
                        //console.log('got this crew', newCrew);
                        distributeToKafka(user).then(results => done(results[0],results[1]));
                    }
                });
            }          

            else {
                let rowDecoded = decodeURI(row.user);
                const userString = JSON.parse(rowDecoded);
                let userIsModified = false;
                for (let key in user) {
                    // skip loop if the property is from prototype
                    if (!user.hasOwnProperty(key)) continue;
                    if (String(user[key]) == String(userString[key])) {
                        continue;
                    }

                    else {
                        /**/
                        userIsModified = true;
                        break;
                    }
                }

                if (userIsModified) {
                    for (let prop in user) {
                        if (!!user[prop] && user[prop].indexOf('\'') !== -1) {
                            console.log(`found something ${user[prop]}`)
                            user[prop] = [user[prop].slice(0,user[prop].indexOf('\'')), "'", user[prop].slice(user[prop].indexOf('\''))].join('');
                        }
                    }
                    let userToString = JSON.stringify(user);
                    encodedUserString = encodeURI(userToString);
                    db.get(`update adonisUserStore_v2 set teammemberNumber=${user.teammemberNumber}, sequenceno='${user.sequenceno}', user='${encodedUserString}' where teammemberNumber='${user.teammemberNumber}'`, function(err,row) {                            
                        if (err) {
                            console.log('get an err inserting ' + user.teamMemberNumber);
                            console.log(err);
                            return done(err,null);
                        }
                    
                        else {
                            //console.log('got this crew', newCrew);
                            distributeToKafka(user).then(results => done(results[0],results[1]));
                        }
                    });
                }

                else {
                    console.log(`user ${user.teammemberNumber} was not updated`);
                    return done(null, `user ${user.teammemberNumber} was not updated`);
                }
            }
        }
    });
}

//push dupe users into an array and send to slack
//make changes to payload and see if updates are handled correctly
function processUsers(users) {
    let uniqueUsers = [users[0]];
    let dupes = [];
    for (let i = 1; i < users.length; i++) {
        let unique = true;
        for (let j = 0; j < uniqueUsers.length; j++) {
            if (users[i].teammemberNumber == uniqueUsers[j].teammemberNumber && users[i].sequenceno == uniqueUsers[j].sequenceno) {
                console.log('got a dupe', users[i].teammemberNumber);
                dupes.push(users[i].teammemberNumber);
                uniqueUsers.splice(j, 1);
                unique = false;
                break;
            }
        }

        if (unique) {
            uniqueUsers.push(users[i]);
        }
    }
    
    //console.log('crew to kafka',util.inspect(users, { maxArrayLength: null,showHidden: false, depth: null }));
    _async.mapSeries(uniqueUsers, getDelta, function(err,results) {
        if (err) {
            console.log(err);
        }

        else {
            console.log("success: ",util.inspect(results, { maxArrayLength: null,showHidden: false, depth: null }));
            console.log("success: ", results.length);
        }
    });
}

const main = new CronJob('0 */59 * * * *', function() {
    console.log('starting main function loop');
    getAdonisUsers().then(users => processUsers(users));
});

main.start();