class crewProfile {
    constructor(chosenUser) {
        const moment = require('moment');
        const uuidv1 = require('uuid/v1');
        this.ci = "";
        this.source = null;
        this.ts = new Date();
        this.n = "teammember:info";
        this.cn = "teammember-service";
        this.tg = "teammember:updated";
        this.plt = "json";
        this.mrc = 3;
        this.rc = 0;
        this.lex = null;
        this.t = null;
        this.ri = 1800000;
        this.k = null;
        this.pl = {};
        this.pl.teamMemberNumber = chosenUser.teammemberNumber;
        this.pl.RFID = chosenUser.RFID || 0;
        this.pl.positionServiceLocation = "";
        this.pl.departmentKey = chosenUser.departmentKey;
        this.pl.safetyNumber = chosenUser.safetyNo;
        this.pl.chargeID = chosenUser.chargeID || 0;
        this.pl.titleCode = chosenUser.titleCode;
        this.pl.firstName = chosenUser.firstName;
        this.pl.middleName = chosenUser.middleName || '';
        this.pl.lastName = chosenUser.lastName;
        this.pl.stateroom = chosenUser.stateroom == '' ? '' : chosenUser.stateroom.split('-')[0];
        this.pl.birthDate = chosenUser.birthDate == '' ? '' : moment(chosenUser.birthDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS');
        this.pl.citizenshipCountrycode = chosenUser.citizenshipCountrycode;
        this.pl.gender = chosenUser.gender;
        this.pl.email = chosenUser.email;
        this.pl.planned = chosenUser.planned;
        this.pl.joiningDate = chosenUser.joiningDate == '' ? '' : moment(chosenUser.joiningDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS');
        this.pl.embarkDate = chosenUser.embarkDate == '' ? '' : moment(chosenUser.embarkDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS');
        this.pl.debarkDate = chosenUser.estimateSignOffDate == '' ? '2112-12-12T00:00:00' : moment(chosenUser.estimateSignOffDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS');
        this.pl.crewPhoto = "";
        this.pl.photoModifiedDate = chosenUser.photoModifiedDate == '' ? chosenUser.photoModifiedDate : moment(chosenUser.photoModifiedDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DDTHH:MM:SS');
        this.pl.teamMemberUserName = chosenUser.teamMemberUserName;
        this.pl.teamMemberDuties = [];
        this.pl.NotcheckedIn = chosenUser.planned;
        this.pl.teamMemberRoles = [
            {
                "teamMemberRoleName":chosenUser.Position,
                "roleCode": chosenUser.PositionID
            }
        ];
        this.pl.phones = [
            {
                "phoneTypeKey": "mobile",
                "number": chosenUser.mobilephone
            }
        ];
        this.pl.visas = [
            {
                "visaTypeCode": chosenUser.c1d_visaTypeCode,
                "number": chosenUser.c1d_visaNumber,
                "issueCountryCode": chosenUser.c1d_issueCountryCode,    
                "issueDate": chosenUser.c1d_issueDate,
                "expiryDate": chosenUser.c1d_expiryDate,
                "numberOfEntries":null,         
                "mediaItemId":null,         
                "isApproved":true
            },
            {
                "visaTypeCode": chosenUser.b1b2_visaTypeCode,
                "number": chosenUser.b1b2_visaNumber,
                "issueCountryCode": chosenUser.b1b2_issueCountryCode,    
                "issueDate": chosenUser.b1b2_issueDate,
                "expiryDate": chosenUser.b1b2_expiryDate,
                "numberOfEntries":null,         
                "mediaItemId":null,         
                "isApproved":true
            },
            {
                "visaTypeCode": chosenUser.schg_visaTypeCode,
                "number": chosenUser.schg_visaNumber,   
                "issueCountryCode": chosenUser.schg_issueCountryCode,    
                "issueDate": chosenUser.schg_issueDate,
                "expiryDate": chosenUser.schg_expiryDate,
                "numberOfEntries":null,         
                "mediaItemId":null,         
                "isApproved":true
            },
            {
                "visaTypeCode": chosenUser.dsbk_visaTypeCode,
                "number": chosenUser.dsbk_visaNumber,   
                "issueCountryCode": chosenUser.dsbk_issueCountryCode,    
                "issueDate": chosenUser.dsbk_issueDate,
                "expiryDate": chosenUser.dsbk_expiryDate,
                "numberOfEntries":null,         
                "mediaItemId":null,         
                "isApproved":true
            },
            {
                "visaTypeCode": chosenUser.smbk_visaTypeCode,
                "number": chosenUser.smbk_visaNumber,   
                "issueCountryCode": chosenUser.smbk_issueCountryCode,    
                "issueDate": chosenUser.smbk_issueDate,
                "expiryDate": chosenUser.smbk_expiryDate,
                "numberOfEntries":null,         
                "mediaItemId":null,         
                "isApproved":true
            }
        ];

        this.pl.identifications =  [
            {        
                "documentTypeCode": "P",        
                "number": chosenUser.pass_visaNumber,        
                "issueCountryCode": chosenUser.pass_issueCountryCode,        
                "expiryDate": chosenUser.pass_expiryDate
            }
        ];

        this.pl.addresses = [      
            {        
                "addressTypeKey": "OTHER",        
                "line1": chosenUser.line1,
                "line2": chosenUser.line2 + ' ' + chosenUser.line3,        
                "city": chosenUser.city,        
                "state": chosenUser.state,        
                "countryCode": chosenUser.addresscountryCode,        
                "zip": chosenUser.zip    
            }
        ];
        this.status = null;
        this.pl.place_of_birth = chosenUser.place_of_birth;
        this.pl.country_of_birth = chosenUser.country_of_birth;
        this.pl.passport_issued_by = chosenUser.passport_issued_by;
        this.pl.passport_issued_city = chosenUser.passport_issued_city;
        this.pl.maritalStatus = chosenUser.maritalStatus;
        this.pl.Vessel = chosenUser.Vessel;
        this.pl.Position = chosenUser.Position;
    }
}

module.exports = crewProfile;